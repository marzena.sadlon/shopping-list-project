package org.example;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ShoppingService {


    private final List<ShoppingList> shoppingLists;

    public ShoppingService() {
        this.shoppingLists = new ArrayList<>();
    }

    public List<ShoppingList> getShoppingLists() {
        return shoppingLists;
    }

    public void add(LocalDate executionDate, List<ShoppingItem> shoppingItems) {
        if (shoppingLists.size() >= 5) {
            System.out.println("Lista jest pełna.");
        } else {
            ShoppingList shoppingList = new ShoppingList(executionDate);
            for (ShoppingItem shoppingItem : shoppingItems) {
                shoppingList.addProduct(shoppingItem);
            }
            shoppingLists.add(shoppingList);
        }
    }
}


