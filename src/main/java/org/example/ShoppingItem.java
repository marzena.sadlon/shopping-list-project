package org.example;

import java.util.Objects;

public class ShoppingItem {


    private final String nameProduct;
    private final int quantity;


    public ShoppingItem(String nameProduct, int quantity) {
        this.nameProduct = nameProduct;
        this.quantity = quantity;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingItem that = (ShoppingItem) o;
        return quantity == that.quantity && Objects.equals(nameProduct, that.nameProduct);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nameProduct, quantity);
    }
}
