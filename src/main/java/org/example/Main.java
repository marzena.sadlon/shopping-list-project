package org.example;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
        ShoppingService shoppingService = new ShoppingService();

        List<ShoppingItem> shoppingItems = List.of(
                new ShoppingItem("bułka", 5),
                new ShoppingItem("ananas", 9),
                new ShoppingItem("cola", 2),
                new ShoppingItem("ser", 1));

        shoppingService.add(LocalDate.parse("2022-05-12"), shoppingItems);

        List<ShoppingItem> shoppingItems1 = List.of(
                new ShoppingItem("chleb", 1),
                new ShoppingItem("masło", 1));

        shoppingService.add(LocalDate.parse("2022-05-15"), shoppingItems1);

        List<ShoppingItem> shoppingItems2 = List.of(
                new ShoppingItem("kabanosy", 4),
                new ShoppingItem("szynka", 1),
                new ShoppingItem("boczek", 3));

        shoppingService.add(LocalDate.parse("2022-05-19"), shoppingItems2);

        List<ShoppingItem> shoppingItems3 = List.of(
                new ShoppingItem("jajka", 10),
                new ShoppingItem("pieczarki", 2),
                new ShoppingItem("pomarańcze", 8),
                new ShoppingItem("mięso mielone", 1),
                new ShoppingItem("marchewka", 7));

        shoppingService.add(LocalDate.parse("2022-05-22"), shoppingItems3);

        List<ShoppingList> shoppingLists = shoppingService.getShoppingLists();

        for (ShoppingList shoppingList : shoppingLists) {
            shoppingList.saveToFile();
            shoppingList.displayProduct();
        }

        ShoppingList shoppingList = shoppingLists.get(0);

        shoppingList.removeProduct(new ShoppingItem("cola", 2));
        shoppingList.displayProduct();

        ShoppingList shoppingList3 = shoppingLists.get(3);
        shoppingList3.modifyItem(new ShoppingItem("jajka", 10), (new ShoppingItem("chleb", 1)));
        shoppingList3.displayProduct();

        ShoppingList shoppingList2 = shoppingLists.get(2);
        List<String> strings = shoppingList2.readFromFile();
        System.out.println(strings);
    }
}



