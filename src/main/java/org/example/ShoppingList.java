package org.example;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ShoppingList {

    private final List<ShoppingItem> shoppingItems;
    private final LocalDate executionDate;

    public ShoppingList(LocalDate executionDate) {
        this.shoppingItems = new ArrayList<>();
        this.executionDate = executionDate;
    }


    public void addProduct(ShoppingItem shoppingItem) {
        shoppingItems.add(shoppingItem);
    }

    public void displayProduct() {
        System.out.println(executionDate);
        for (int i = 0; i < shoppingItems.size(); i++) {
            System.out.println(shoppingItems.get(i).getNameProduct() + "  " + shoppingItems.get(i).getQuantity());
        }
        System.out.println();
    }

    public void removeProduct(ShoppingItem shoppingItem) {
        shoppingItems.remove(shoppingItem);
    }


    public void modifyItem(ShoppingItem oldItem, ShoppingItem newItem) {
        int index = this.shoppingItems.indexOf(oldItem);
        if (index >= 0) {
            this.shoppingItems.set(index, newItem);
        }
    }

    public void saveToFile() throws IOException {
        Path path = Paths.get("lists", "shopping-list-" + this.executionDate + ".txt");
        Files.createDirectories(path.getParent());

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(path.toFile()))) {
            for (ShoppingItem item : shoppingItems) {
                writer.write(item.getNameProduct() + " - " + item.getQuantity() + "\n");
            }
        }
    }

    public List<String> readFromFile() throws IOException {
        Path path = Paths.get("lists", "shopping-list-" + this.executionDate + ".txt");
        return Files.readAllLines(path);
    }
}